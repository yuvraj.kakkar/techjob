import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, HostListener } from '@angular/core';

import { Router } from '@angular/router';

import { DashboardService } from './dashboard.service';
import { GlobalVariablesService, HttpClientService } from '../services';

// GOOGLE
declare const gapi: any;

// FACEBOOK
declare var FB: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  url = this.globalVar.apiurl;
  GOOGLE
  @ViewChild('googleLogin')
  public googleLogin: ElementRef;
  public auth2: any;

  FACEBOOK
  userId;

  SCROLL
  yOffset = 0;
  showClass = false;

  @HostListener('window:scroll', ['$event'])
   scrollHandler(event) {
     this.yOffset = window.pageXOffset;
    //  230
     if(window.pageYOffset > 400) {
       this.showClass = true;
     }
     if(window.pageYOffset < 400) {
       this.showClass = false;
     }
   }

  constructor(
    private mainService: DashboardService,
    private http: HttpClientService,
    private globalVar: GlobalVariablesService,
    private router: Router
  ) { }

  ngOnInit() {
    // FACEBOOK LOGIN
    (window as any).fbAsyncInit = () => {
      FB.init({
        appId: '378569649407490',
        cookie: true,
        xfbml: true,
        version: 'v3.2'
      });
      FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    // FACEBOOK LOGIN ENDS
  }

  purchaseMonthlyNow() {
    // window.open('https://techjobinterview.thinkific.com/users/checkout/auth');
    window.open('https://techjobinterview.thinkific.com/enroll/405629?price_id=423197');
  }

  purchaseAnnualNow() {
    // window.open('https://techjobinterview.thinkific.com/users/checkout/auth');
    window.open('https://techjobinterview.thinkific.com/enroll/405629?price_id=423197');
  }



  // FACEBOOK LOGIN
  submitLogin() {
    console.log('submit login to facebook');
    FB.login();
    FB.login((response) => {
      console.log('submitLogin', response);
      this.userId = response.authResponse.userID;
      if (response.authResponse) {
        console.log(response.authResponse);
        //login success
        //login success code here
        //redirect to home page
        FB.api('/me', {fields: 'name, email'}, function(response) {
          console.log(response);
        });
      } else {
        console.log('User login failed');
      }
    });

  }

  // GOOGLE LOGIN
  public attachSignin(element) {
    let that = this;
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {
        let profile = googleUser.getBasicProfile();
        console.log('Token || ' + googleUser.getAuthResponse().id_token);
        console.log('ID || ' + profile.getId());
        console.log('Name: ' + profile.getName());
        console.log('Image URL:' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail());

        const httpOptions = {
          body: {
            token: googleUser.getAuthResponse().id_token,
            name: profile.getName(),
            email: profile.getEmail(),
            usertype: 'google'
          }
        };
        this.http.post(this.url + 'signup', httpOptions).subscribe((res: any) => {
          console.log(res);
          // if (res.status == 200) {
          //    console.log(res.status);
          // } else {
          //   alert('Un-successfull operation');
          // }
        });
      },
      (error) => {
        console.log(JSON.stringify(error, undefined, 2));
      });
  }

  ngAfterViewInit() {
    gapi.load('auth', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '975233870856-khq7848vgjtk1hae4tmecekdacel4v0c.apps.googleusercontent.com' ,
        // client_id: '692507656868-6nvl1lovcmqaugv3h0h1f0jaaal2if5i.apps.googleusercontent.com',
        cookiepolicy: '',
        scope: ''
      });
      this.attachSignin(this.googleLogin.nativeElement);
    });
  }
  // GOOGLE LOGIN ENDS

}
