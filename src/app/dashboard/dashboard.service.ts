import { Injectable } from '@angular/core';

import { GlobalVariablesService, HttpClientService } from '../services';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  url = this.globalVar.apiurl;

  constructor(
    private http: HttpClientService,
    private globalVar: GlobalVariablesService,
  ) { }

  loginMainService(tokenn, namee, mail) {

    // const header: HttpHeaders = new HttpHeaders()z
    //   .append('Content-Type', 'application/json');

    const httpOptions = {
      // headers: header,
      body: {
        token: tokenn,
        name: namee,
        email: mail
      }
    };

    return this.http.post(this.url, httpOptions);
  }


}
