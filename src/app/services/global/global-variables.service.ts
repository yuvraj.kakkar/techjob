import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalVariablesService {
  private _baseurl: string;
  private _apiurl: string;
  private _imgbaseurl: string;
  private _userurl: string;
  public allmanagers: any[];
  public allcustomers: any[];
  public alldrivers: any[];
  public allbins: any[];
  public allBinSizes: any[];
  public binsInYard;
  public binsAtCustomer;
  public globalCount=0;
  public allUsers:any[];

  constructor() {
    //  this._baseurl = 'http://localhost:8086/';
    //  this._apiurl = 'http://localhost:8086/admin/';
    //  this._imgbaseurl = 'http://localhost:8086/uploads/';


    //  this._baseurl = 'https://bazarna.herokuapp.com/';
    //  this._apiurl = 'https://bazarna.herokuapp.com/api/';
    //  this._imgbaseurl = 'https://bazarna.herokuapp.com/api/upload/';

    // this._baseurl = 'http://192.168.1.4:8081';
    this._apiurl = 'http://192.168.1.15:3000/';
    // this._userurl = 'http://192.168.1.4:8081/user/';

  }

  get baseurl() {
    return this._baseurl;
  }
  get apiurl() {
    return this._apiurl;
  }
  get imgbaseurl() {
    return this._imgbaseurl;
  }

}
